#!/usr/local/bin/scopes
import .vk

define lib
    load-library "/usr/lib/libSDL2.so"
    import-c "lib.c"
        """"
            #include <SDL2/SDL.h>
            #include <SDL2/SDL_vulkan.h>
        '()
using lib filter "^(SDL(.+))$"

if (0 != (SDL_Init (| SDL_INIT_VIDEO SDL_INIT_EVENTS)))
    error! "sdl init"

let window =
    SDL_CreateWindow "Test" 0 0 1280 768 (| SDL_WINDOW_SHOWN SDL_WINDOW_VULKAN)

if (null == window)
    error! "Window creation error"
    
let name = "Test"

let version =
    vk.make-version 1:u32 0:u32 0:u32

let app =
    allocaof
        vk.ApplicationInfo.new
            api-version = version
            application-name = name
            engine-name = name


let inst =
    alloca vk.Instance


let ext-count = (local u32)
let ext-names =
    alloca-array (pointer i8) 5
SDL_Vulkan_GetInstanceExtensions window ext-count ext-names


let inst-info =
    allocaof
        vk.InstanceCreateInfo.new app
            enabled-extension-count = ext-count
            enabled-extension-names = ext-names


vk.createInstance inst-info null inst

let device-count = (local u32)

vk.enumeratePhysicalDevices (inst @ 0) device-count null

let devices =
    alloca-array vk.PhysicalDevice (device-count as i32)

vk.enumeratePhysicalDevices (inst @ 0) device-count devices

#for i in (range 0 device-count)
    let device = (devices @ i)
    let properties =
        alloca vk.PhysicalDeviceProperties
    let features =
        alloca vk.PhysicalDeviceFeatures
    vk.getPhysicalDeviceProperties device properties
    vk.getPhysicalDeviceFeatures device features
    print
        "Physical device suitable: "
        i
        and
            properties.deviceType == vk.PHYSICAL_DEVICE_TYPE_DISCRETE_GPU
            features.geometryShader

let physical-device = (devices @ 0)

let queue-count = (local u32)
vk.getPhysicalDeviceQueueFamilyProperties physical-device queue-count null

let queue-families =
    alloca-array vk.QueueFamilyProperties (queue-count as i32)
vk.getPhysicalDeviceQueueFamilyProperties physical-device queue-count queue-families

let family-index =
    alloca u32

for i in (range 0:u32 queue-count)
    let queue-family = (queue-families @ i)
    if (queue-family.queueCount > 0 and ((queue-family.queueFlags & vk.QUEUE_GRAPHICS_BIT) == 0))
        family-index = i

let device =
    alloca vk.Device

let queue-create-info =
    allocaof
        vk.DeviceQueueCreateInfo.new
            queue-family-index = (family-index as u32)
            queue-count = 1
            queue-priorities = (allocaof 1.0)

let device-create-info =
    allocaof
        vk.DeviceCreateInfo.new
            queue-create-info-count = 1:u32
            queue-create-infos = queue-create-info
            enabled-features = (local vk.PhysicalDeviceFeatures)

if ((vk.createDevice physical-device device-create-info null device) != vk.SUCCESS)
    error! "Device creation error"




