vk.sc
=====

A vulkan wrapper for the [scopes](https://bitbucket.com/Duangle/scopes) programming language.
Usage: Copy the files `casing.sc` and `vk.sc` either to `path/to/lib/scopes/` or to your working directory, and import `vk`.
The scope `vk` contains all vulkan constants, structs and functions.
For constants, the Prefix `VK_` is removed, for structs, the prefix `Vk` is removed, for functions the prefix `vk` is removed, and the first character is now lowercase.

Additionally, the create info structs can be created using their method `new` (`vk.StructName.new arguments`)
This method implicitely fills some fields and allows simpler assignment.
The field `sType` is implicitely set to the correct value.
When assigning the values one after another, the order is the same as in the c header, but `pNext` and `flags` come last, since they are currently just set to default values in most cases.
When assigning the values using keyword-value notation, the keywords are not camelCase, but a lowercase lispy-notation. For pointer types, the trailing `p`s for pointer types are dropped entirely.
If the other values have proper default values, they are implicitely set, when not specified.
Otherwise it will not compile.

__Warning: The notation may change__

_TODO: Define this method for all create info structs_

_TODO: Also add some higher level functions_

Demo
____

There is a demo called `demo.sc`.
It is a executable, so you can just run it using `./demo.sc` after you installed scopes.

SDL2 is used for windowing.

The demo does not work yet.
