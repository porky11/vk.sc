fn uppercase? (c) 
    and
        <= (char "A") c 
        <= c (char "Z")

fn lowercase? (c) 
    and
        <= (char "a") c
        <= c (char "z")

define case-diff
    (char "a") - (char "A")

fn uppercase (c)
    if (lowercase? c)
        c - case-diff
    else c

fn lowercase (c)
    if (uppercase? c)
        c + case-diff
    else c

locals;
