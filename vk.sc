using import .casing

define lib
    load-library "/usr/lib/libvulkan.so"
    import-c "lib.c"
        """"
            #include <vulkan/vulkan.h>
        '()

using lib #filter "^(vk(.+)|VK(.+)|Vk(.+))$"

fn option (val default)
    if (none? val) default
    else val







syntax-extend
    let new-scope = (Scope)
    for k v in syntax-scope
        let string = (k as Symbol as string)
        let first second = (string @ 0) (string @ 1)
        let start =
            if ((uppercase first) == (char "V") and (uppercase second) == (char "K"))
                if (and (uppercase? first) (uppercase? second))
                    3:usize
                else
                    2:usize
            else
                continue;

        
        let new-string =
            if (lowercase? first)
                let c =
                    allocaof
                        lowercase (string @ 2)
                
                ..
                    string-new c 1:usize
                    slice string (start + 1)
            else
                slice string start
        
        set-scope-symbol! new-scope (string->Symbol new-string) v
    fn make-version (maj min pat)
        |
            maj >> 22
            min >> 12
            pat
    set-scope-symbol! new-scope 'make-version make-version
    
    fn application-info (api-version
                         application-name application-version
                         engine-name engine-version
                         next)
        VkApplicationInfo
            sType = VK_STRUCTURE_TYPE_APPLICATION_INFO
            pNext = (option next null)
            apiVersion = api-version
            pApplicationName = (option application-name "")
            applicationVersion = (option application-version 0)
            pEngineName = (option engine-name "")
            engineVersion = (option engine-version 0)

    set-type-symbol! VkApplicationInfo 'new application-info

    fn instance-create-info (application-info
                             enabled-layer-count enabled-layer-names
                             enabled-extension-count enabled-extension-names
                             next flags)
        VkInstanceCreateInfo
            sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO
            pNext = (option next null)
            flags = (option flags 0:u32)
            pApplicationInfo = (option application-info null)
            enabledLayerCount = (option enabled-layer-count 0:u32)
            ppEnabledLayerNames = (option enabled-layer-names null)
            enabledExtensionCount = (option enabled-extension-count 0:u32)
            ppEnabledExtensionNames = (option enabled-extension-names null)

    set-type-symbol! VkInstanceCreateInfo 'new instance-create-info

    fn device-queue-create-info (queue-family-index
                                 queue-count queue-priorities
                                 next flags)
        VkDeviceQueueCreateInfo
            sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO
            pNext = (option next null)
            flags = (option flags 0:u32)
            queueFamilyIndex = queue-family-index
            queueCount = queue-count
            pQueuePriorities = queue-priorities

    set-type-symbol! VkDeviceQueueCreateInfo 'new device-queue-create-info

    fn device-create-info (queue-create-info-count queue-create-infos
                           enabled-layer-count enabled-layer-names
                           enabled-extension-count enabled-extension-names
                           enabled-features
                           next flags)
        VkDeviceCreateInfo
            sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO
            pNext = (option next null)
            flags = (option flags 0:u32)
            queueCreateInfoCount = queue-create-info-count
            pQueueCreateInfos = queue-create-infos
            enabledLayerCount = (option enabled-layer-count 0:u32)
            ppEnabledLayerNames = (option enabled-layer-names null)
            enabledExtensionCount = (option enabled-extension-count 0:u32)
            ppEnabledExtensionNames = (option enabled-extension-names null)
            pEnabledFeatures = (option enabled-features null)


    set-type-symbol! VkDeviceCreateInfo 'new device-create-info

    set-scope-symbol! syntax-scope 'vk new-scope
    syntax-scope

vk

